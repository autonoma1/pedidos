from marshmallow import fields, EXCLUDE, post_load
from project import ma
from project.pedidos.models import Pedido, Usuario


class PedidoSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Pedido
        load_instance = True


class UsuarioSchema(ma.Schema):
    id = fields.Int(required=True)
    email = fields.Email(required=True)
    name = fields.Str(required=True)

    class Meta:
        unknown = EXCLUDE

    @post_load
    def make_user(self, data, **kwargs):
        return Usuario(**data)


pedido_schema = PedidoSchema()
usuario_schema = UsuarioSchema()
